class SelectionSort

  def initialize(array)
    @toSort = array
    @sortedItem = []
    @current_index = 0
    @iteration = 0
    @done = false

    @last_min = @toSort.first
    @last_min_index = 0
    @current_min = -1
    @current_min_index = -1
  end

  def execute
    unless @done
      until @done do
        algo
      end
    end
   { iteration: @iteration, sorted: @toSort }
  end

  def algo

    if (@last_min_index) != @toSort.length
      @current_min_index = @last_min_index
      @current_min = @toSort[@current_min_index]
      index =  @current_min_index
      #showDebug
      until index == (@toSort.length - 1) do
        index += 1
        if (@toSort[index] < @current_min)
          @current_min_index = index
          @current_min = @toSort[index]
        end
        @iteration += 1
      end
      swap @toSort, @current_min_index, (@last_min_index)
      @last_min = @current_min
      @last_min_index += 1
    else
      @done = true
    end
  end

  def showDebug
    p "current min index : #{@current_min_index}"
    p "current min : #{@current_min}"
    p "last min index : #{@last_min_index}"
    p "last min : #{@last_min}"
    p "#{@toSort}"
    p "============================================================================"
  end

  def swap(array, dest, from)
    array[from], array[dest] = array[dest], array[from]
  end

  def newArray(array)
    @toSort = array
    @sortedItem = []
    @iteration = 0
    @done = false
  end

end