require_relative "SelectionSort"
require_relative "InsertionSort"
require_relative "BubbleSort"
require_relative "MergeSort"
require_relative 'fileParser'

class Sorter
  def initialize(env)
    @parsedFile = env[:file]
    @selectionSort = SelectionSort.new(@parsedFile.getParsedContent.dup)
    @insertionSort = InsertionSort.new(@parsedFile.getParsedContent.dup)
    @bubbleSort = BubbleSort.new(@parsedFile.getParsedContent.dup)
    @mergeSort = MergeSort.new(@parsedFile.getParsedContent.dup)
  end

  def start
    puts "#{@parsedFile.getParsedContent.length} elements"
    selectionSort
    insertionSort
    bubbleSort
    mergeSort
  end

  def newFile(filePath)
    @parsedFile = ParsedFile.new(filePath)
    if @parsedFile.isOpen
      @selectionSort = SelectionSort.new(@parsedFile.getParsedContent.dup)
      @insertionSort = InsertionSort.new(@parsedFile.getParsedContent.dup)
      @bubbleSort = BubbleSort.new(@parsedFile.getParsedContent.dup)
      @mergeSort = MergeSort.new(@parsedFile.getParsedContent.dup)
    else
      puts "Cannot open the file, sorting algorithm will continue to work with the old one"
    end
  end

  def selectionSort
    result = @selectionSort.execute()
    p result
    puts "select sort: #{result[:iteration]} comparisons"
  end

  def insertionSort
    result = @insertionSort.execute()
    p result
    puts "insertion sort: #{result[:iteration]} comparisons"

  end

  def bubbleSort
    result = @bubbleSort.execute()
    p result
    puts "bubble sort: #{result[:iteration]} comparisons"
  end

  def mergeSort
    result = @mergeSort.execute()
    p result
    puts "merge sort: #{result[:iteration]} comparisons"
  end
end