class BubbleSort

  def initialize(array)
    @toSort = array
    @iteration = 0
    @sorted = 0
    @done = false
  end

  def execute
    while @done == false
      @iteration += 1
      algo
    end
    { iteration: @iteration, sorted: @toSort }
  end

  def algo
    isSorted = true
    index = 0
    while index != (@toSort.length - 1 - @sorted) do
      @iteration += 1
      if @toSort[index] > @toSort[index + 1]
        swap(@toSort, index, index + 1)
        isSorted = false
      end
      index += 1
    end
    @sorted += 1
    @done = true if isSorted
  end

  def swap(array, dest, from)
    array[from], array[dest] = array[dest], array[from]
  end

  def showDebug
    p "current min index : #{@current_min_index}"
    p "current min : #{@current_min}"
    p "last min index : #{@last_min_index}"
    p "last min : #{@last_min}"
    p "#{@toSort}"
    p "============================================================================"
  end

  def newArray(array)
    @toSort = array
    @iteration = 0
    @done = false
  end

end