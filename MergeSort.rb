class MergeSort

  def initialize(array)
    @toSort = array
    @iteration = 0
    @sorted = []
    @done = false
  end

  def execute
    @sorted = algo(@toSort.dup)

    { iteration: @iteration, sorted: @sorted }
  end

  def algo(array)
    return array if array.count <= 1
    splitedArray = array.count / 2
    firstPart = algo array.slice(0, splitedArray)
    secondPart = algo array.slice(splitedArray, array.count - splitedArray)

    array = []
    firstOffset = 0
    secondOffset = 0
    while firstOffset < firstPart.length && secondOffset < secondPart.length
      firstNumber = firstPart[firstOffset]
      secondNumber = secondPart[secondOffset]
      @iteration += 1
      if firstNumber <= secondNumber
        array << firstNumber
        firstOffset += 1
      else
        array << secondNumber
        secondOffset += 1
      end
    end

    while firstOffset < firstPart.length
      array << firstPart[firstOffset]
      firstOffset += 1
    end

    while secondOffset < secondPart.length
      array << secondPart[secondOffset]
      secondOffset += 1
    end
    array
  end

  def swap(array, dest, from)
    array[from], array[dest] = array[dest], array[from]
  end

  def showDebug
    p "current min index : #{@current_min_index}"
    p "current min : #{@current_min}"
    p "last min index : #{@last_min_index}"
    p "last min : #{@last_min}"
    p "#{@toSort}"
    p "============================================================================"
  end

  def newArray(array)
    @toSort = array
    @iteration = 0
    @done = false
  end

end