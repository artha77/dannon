class ParsedFile
  def initialize(filePath)
    @file = File.open(filePath, 'r') rescue nil
    @rawContent = @file ? @file.read : nil
    @parsedContent = []
    parseFile
  end

  def getRawContent
    if self.isOpen
      @rawContent
    else
      nil
    end
  end

  def getParsedContent
    if self.isOpen
      @parsedContent
    else
      []
    end
  end

  def isOpen
    !@file.nil?
  end

  def parseFile(strategy = :errorWhenNil)
    if self.isOpen
      @rawContent.split(' ').each { |item| @parsedContent.push( parseItem(item)) }
      applyStrategy strategy
    end
  end

  def parseItem(item)
    Float(item) rescue nil
  end

  def exitWithError
    STDERR.write("File Malformated\n")
    exit(84)
  end

  def applyStrategy(strategy)
    if strategy == :errorWhenNil
      exitWithError if @parsedContent.include?(nil)
    elsif strategy == :ignoreNil
      :doNothing
    elsif strategy == :removeNil
      @parsedContent.delete_if { |item| item.nil? }
    end
  end

end