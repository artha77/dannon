class InsertionSort

  def initialize(array)
    @toSort = array
    @iteration = 0
    @done = false
  end

  def execute
    unless @done
      algo
    end
    { iteration: @iteration, sorted: @toSort }
  end

  def algo
    final = [@toSort[0]]
    @toSort.delete_at(0)
    for i in @toSort
      final_index = 0
      while final_index < final.length
        @iteration += 1
        if i <= final[final_index]
          final.insert(final_index,i)
          break
        elsif final_index == final.length-1
          final.insert(final_index+1,i)
          break
        end
        final_index += 1
      end
    end
    @done = true
    @toSort = final
  end

  def showDebug
    p "current min index : #{@current_min_index}"
    p "current min : #{@current_min}"
    p "last min index : #{@last_min_index}"
    p "last min : #{@last_min}"
    p "#{@toSort}"
    p "============================================================================"
  end

  def newArray(array)
    @toSort = array
    @iteration = 0
    @done = false
  end

end