require_relative 'fileParser'

class Arguments

  def initialize()
    raise "Not instanciable."
  end

  def self.parse(args = ARGV)
    if args.first == "-h"
      printUsage
    elsif haveError?
      printUsage
      :error
    else
      parsedFile = ParsedFile.new(args.first)
      {file: parsedFile}
    end
  end

  def self.printUsage
    puts "USAGE"
    puts "\t\t./206neutrinos n a h sd"
    puts "DESCRIPTION"
    puts "\t\tn\tnumber of values"
    puts "\t\ta\tarithmetic mean"
    puts "\t\th\tharmonic mean"
    puts "\t\tsd\tstandard deviation"
    :usage
  end

  def self.haveError?(args = ARGV)
    false
  end

  def self.is_number?(*args)
    args.each do |item|
      item = Integer(item) rescue false
      return false unless item
    end
    true
  end

  private_class_method :printUsage, :haveError?, :is_number?
end