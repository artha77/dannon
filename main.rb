require_relative 'argument'
require_relative 'Sorter'

env = Arguments.parse(ARGV)
if env != :usage && env != :error
  sorter = Sorter.new(env)
  sorter.start
end
exit(84) if env == :error